# Medusa Temperature & Humidity Sensor - DHT11

![Medusa Temperature & Humidity Sensor - DHT11](https://shop.edwinrobotics.com/4143-thickbox_default/medusa-temperature-humidity-sensor-dht11.jpg "Medusa Temperature & Humidity Sensor - DHT11")

[Medusa Temperature & Humidity Sensor - DHT11](https://shop.edwinrobotics.com/modules/1235-medusa-temperature-humidity-sensor-dht11.html)


The Medusa Temperature and Humidity Sensor utilizes the DHT11, a single wire factory calibrated digital sensor. Use the Adafruit Library to interface with the Arduino.
 
Specifications:

Power supply: DC 3.5～5.5V
Supply Current: measurement 0.3mA standby 60μA
Sampling period: more than 2 seconds
RH Accuracy: At 25℃ ±5% RH
Temperature Repeatability: ±0.2℃


Repository Contents
-------------------
* **/Design** - Eagle Design Files (.brd,.sch)
* **/Panel** - Panel file used for production

License Information
-------------------
The hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).

Distributed as-is; no warranty is given.